# Sabaidea Code Challenge
In this Task only the required features are developed, and developing features causing complexities are avoided.

## Methods and Classes
This sections contains a comprehensive list of Methods and classes used to develop the project and the reason for their implementation:

### Navigation Component
Since the project is considered Single Activity, it is developed using the Navigation Component from the start.

### BaseFragment
This class is defined generically and all fragments related to ViewBinding take place in this class while the required variables are provided for the child.

### BaseAdapter
This class is implemented for handling activities that are related to lists

### ViewModel
 It is important to point out that ViewModel is only shared on the main graph which is the application's graph. Also, In order to use the repository layer, the object is passed to the viewModel constructor using Hilt to reserve states and data collection.
### Retrofit
For the Http requests, retrofit is used and all files that are related to it are collected in _network_ package.
also the response is mapped with Mapper class to application's native model

### Concurrency
For the concurrency corutine is used in this project. 
all request are excute in IO dispather which can be found in repository class
