package ir.sabaide.codecahllenge.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.sabaide.codecahllenge.domain.movie.Movie
import ir.sabaide.codecahllenge.repository.MovieRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel
@Inject constructor(private val repository: MovieRepository) : ViewModel() {

    private val _movieListLiveData: MutableLiveData<List<Movie>> = MutableLiveData()

    val movieListLiveData: LiveData<List<Movie>>
        get() {
            return _movieListLiveData
        }

    fun search(query: String) {
        viewModelScope.launch {
            val response = repository.searchMovie(query)
            response?.let { movies ->
                _movieListLiveData.value = movies
            }
        }
    }
}