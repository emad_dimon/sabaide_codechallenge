package ir.sabaide.codecahllenge.ui.search

import android.view.LayoutInflater
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import ir.sabaide.codecahllenge.R
import ir.sabaide.codecahllenge.databinding.FragmentMovieBinding
import ir.sabaide.codecahllenge.utiles.baseClasses.BaseFragment

@AndroidEntryPoint
class SearchFragment: BaseFragment<FragmentMovieBinding>() {
    private val viewModel: SearchViewModel by hiltNavGraphViewModels(R.id.main_graph)
    private val adapter: SearchListAdapter = SearchListAdapter()

    override fun observer() {
        with(viewModel) {
            movieListLiveData.observe(viewLifecycleOwner) { movies ->
                adapter.items = movies
            }
        }
    }

    override fun bind(inflater: LayoutInflater): FragmentMovieBinding
    = FragmentMovieBinding.inflate(inflater)

    override fun initView() {
        binding.movieList.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.movieList.adapter = adapter

        binding.search.setOnClickListener {
            viewModel.search(binding.searchBox.text.toString())
        }
    }
}