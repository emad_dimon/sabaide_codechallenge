package ir.sabaide.codecahllenge.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import ir.sabaide.codecahllenge.databinding.ItemMovieBinding
import ir.sabaide.codecahllenge.domain.movie.Movie
import ir.sabaide.codecahllenge.utiles.baseClasses.BaseRecyclerAdapter
import ir.sabaide.codecahllenge.utiles.baseClasses.BaseViewHolder

class SearchListAdapter : BaseRecyclerAdapter<Movie, SearchListAdapter.ViewHolder>() {

    override fun inflate(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    class ViewHolder(private val binding: ItemMovieBinding) : BaseViewHolder<Movie>(binding.root) {
        override fun bind(t: Movie, position: Int) {
            binding.description.text = t.description.trim()
            binding.title.text = t.title.trim()

            Glide.with(binding.thumbnail.context)
                .load(t.image.mediumImage)
                .centerCrop()
                .into(binding.thumbnail)
        }
    }

    override fun areItemsSame(oldList: Movie?, newList: Movie?): Boolean {
        return oldList?.uid == newList?.uid
    }
}