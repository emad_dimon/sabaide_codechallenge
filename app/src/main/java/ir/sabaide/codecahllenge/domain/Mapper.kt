package ir.sabaide.codecahllenge.domain

interface Mapper<ResponseModel, DomainModel> {
    fun entityToDomainModel(entity: ResponseModel): DomainModel
    fun domainModelToEntity(model: DomainModel): ResponseModel
}