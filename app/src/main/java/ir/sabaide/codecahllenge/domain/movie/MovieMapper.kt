package ir.sabaide.codecahllenge.domain.movie

import ir.sabaide.codecahllenge.domain.Mapper
import ir.sabaide.codecahllenge.domain.image.ImageMapper
import ir.sabaide.codecahllenge.network.responose.AttributesResponse
import ir.sabaide.codecahllenge.network.responose.MovieResponse

class MovieMapper: Mapper<MovieResponse, Movie> {
    override fun entityToDomainModel(entity: MovieResponse): Movie {
        return Movie(
            entity.uid,
            entity.description,
            entity.title,
            ImageMapper().entityToDomainModel(entity.image)
        )
    }

    override fun domainModelToEntity(model: Movie): MovieResponse {
        return MovieResponse(
            model.uid,
            model.description,
            model.title,
            ImageMapper().domainModelToEntity(model.image)
        )
    }

    fun entitiesToListOfDomain(list: List<AttributesResponse>): List<Movie> {
        return list.map { entityToDomainModel(it.attributes) }
    }
}