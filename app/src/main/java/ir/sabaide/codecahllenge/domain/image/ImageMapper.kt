package ir.sabaide.codecahllenge.domain.image

import ir.sabaide.codecahllenge.domain.Mapper
import ir.sabaide.codecahllenge.network.responose.PicResponse

class ImageMapper: Mapper<PicResponse, Pic> {
    override fun entityToDomainModel(entity: PicResponse): Pic {
        return Pic(
            entity.smallImage,
            entity.mediumImage,
            entity.bigImage
        )
    }

    override fun domainModelToEntity(model: Pic): PicResponse {
        return PicResponse(
            model.smallImage,
            model.mediumImage,
            model.bigImage
        )
    }
}