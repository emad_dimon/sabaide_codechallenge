package ir.sabaide.codecahllenge.domain.image

data class Pic(
    val smallImage: String,
    val mediumImage: String,
    val bigImage: String
)