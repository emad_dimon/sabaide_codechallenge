package ir.sabaide.codecahllenge.domain.movie

import ir.sabaide.codecahllenge.domain.image.Pic

data class Movie(
    val uid: String,
    val description: String,
    val title: String,
    val image: Pic
)