package ir.sabaide.codecahllenge.repository

import ir.sabaide.codecahllenge.domain.movie.MovieMapper
import ir.sabaide.codecahllenge.network.AppClient
import ir.sabaide.codecahllenge.network.services.MovieService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class MovieRepository
@Inject constructor(private val appClient: AppClient) {

    suspend fun searchMovie(query: String) =
        withContext(Dispatchers.IO) {
            try {
                val baseResponse = appClient.getService(MovieService.V1::class.java)
                    .search(query)

                MovieMapper().entitiesToListOfDomain(baseResponse.data)
            } catch (e: Exception) {
                null
            }
        }
}