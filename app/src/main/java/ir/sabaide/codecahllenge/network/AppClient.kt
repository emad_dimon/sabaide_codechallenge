package ir.sabaide.codecahllenge.network

import ir.sabaide.codecahllenge.BuildConfig
import ir.sabaide.codecahllenge.network.utiles.GCFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Inject

class AppClient
@Inject
constructor(private val okHttpClient: OkHttpClient, private val gson: GCFactory) {

    private val getClient by lazy {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(gson.gsonConverterFactory())
            .build()
    }

    fun <T> getService(service: Class<T>): T {
        return getClient.create(service)
    }

    fun cancelAllRequest() {
        okHttpClient.dispatcher.cancelAll()
    }
}