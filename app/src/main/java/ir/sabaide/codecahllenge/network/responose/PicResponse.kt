package ir.sabaide.codecahllenge.network.responose

import com.google.gson.annotations.SerializedName

data class PicResponse(
    @SerializedName("movie_img_s")
    val smallImage: String,

    @SerializedName("movie_img_m")
    val mediumImage: String,

    @SerializedName("movie_img_b")
    val bigImage: String
)