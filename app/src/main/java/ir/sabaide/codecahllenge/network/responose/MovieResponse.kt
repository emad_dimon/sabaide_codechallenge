package ir.sabaide.codecahllenge.network.responose

import com.google.gson.annotations.SerializedName

data class MovieResponse(
    val uid: String,

    @SerializedName("descr")
    val description: String,

    @SerializedName("movie_title")
    val title: String,

    @SerializedName("pic")
    val image: PicResponse
)