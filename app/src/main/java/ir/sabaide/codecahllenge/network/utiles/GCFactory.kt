package ir.sabaide.codecahllenge.network.utiles
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class GCFactory
@Inject constructor() {
    private val gson: Gson = GsonBuilder().create()

    fun gsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }
}