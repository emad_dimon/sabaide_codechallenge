package ir.sabaide.codecahllenge.network.services

import ir.sabaide.codecahllenge.network.responose.AttributesResponse
import ir.sabaide.codecahllenge.network.responose.BaseModel
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieService {
    interface V1 {
        @GET("api/en/v1/movie/movie/list/tagid/1000300/text/{search}/sug/on")
        suspend fun search(@Path("search") query: String): BaseModel<AttributesResponse>
    }
}