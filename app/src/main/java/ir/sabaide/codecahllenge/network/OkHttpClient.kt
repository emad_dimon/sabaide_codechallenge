package ir.sabaide.codecahllenge.network

import ir.sabaide.codecahllenge.network.interceptors.CacheInterceptor
import ir.sabaide.codecahllenge.network.interceptors.HeaderInterceptor
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class OkHttpClient
@Inject
constructor(val headerInterceptor: HeaderInterceptor,
            val cacheInterceptor: CacheInterceptor) {

    private val TIMEOUT_SECONDS = 20L

    fun build(): okhttp3.OkHttpClient {
        val okHttp = okhttp3.OkHttpClient.Builder()
        okHttp.connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .addInterceptor(headerInterceptor)
            .addInterceptor(cacheInterceptor)


        return okHttp.build()
    }


    private val loggingInterceptor = run {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
    }
}