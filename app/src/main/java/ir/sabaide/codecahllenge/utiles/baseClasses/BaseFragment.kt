package ir.sabaide.codecahllenge.utiles.baseClasses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<T : ViewBinding> : Fragment() {
    private var _binding: T? = null
    protected val binding: T
        get() {
            return _binding!!
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bind(inflater)
        initView()
        observer()

        return (_binding as ViewBinding).root
    }

    abstract fun observer()
    abstract fun bind(inflater: LayoutInflater): T
    abstract fun initView()
}