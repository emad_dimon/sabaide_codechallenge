package ir.sabaide.codecahllenge.utiles.baseClasses

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView) {
    protected val context: Context by lazy {
        itemView.context
    }

    abstract fun bind(t: T, position: Int)
}