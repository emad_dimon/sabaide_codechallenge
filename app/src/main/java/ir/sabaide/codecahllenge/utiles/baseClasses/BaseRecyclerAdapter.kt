package ir.sabaide.codecahllenge.utiles.baseClasses

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T, VH : BaseViewHolder<T>> : RecyclerView.Adapter<VH>() {

    private var baseAdapterItems: MutableList<T?> = ArrayList()

    var items: List<T?>?
        get() {
            return baseAdapterItems
        }
        set(value) {
            if (value != null) {
                val diffUtil = DiffCallBack(baseAdapterItems, value)
                val diffResult = DiffUtil.calculateDiff(diffUtil)

                baseAdapterItems.clear()
                baseAdapterItems.addAll(value)

                diffResult.dispatchUpdatesTo(this)
            } else {
                notifyItemRangeRemoved(0, baseAdapterItems.size)
                baseAdapterItems.clear()
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return inflate(parent, viewType)
    }

    override fun getItemCount(): Int = baseAdapterItems.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        items?.get(position)?.let { holder.bind(it, holder.adapterPosition) }
    }

    abstract fun inflate(parent: ViewGroup, viewType: Int): VH

    fun getItem(position: Int = -1): T? {
        return if (position == -1)
            items?.get(baseAdapterItems.size - 1)
        else
            items?.get(position)
    }

    fun addItem(item: T?, position: Int = -1) {
        if (position == -1) {
            baseAdapterItems.add(item)
            notifyItemInserted(baseAdapterItems.size)
        } else {
            baseAdapterItems.add(position, item)
            notifyItemInserted(position)
        }
    }

    protected abstract fun areItemsSame(oldList: T?, newList: T?): Boolean

    inner class DiffCallBack(private val oldList: MutableList<T?>, private val newList: List<T?>) :
        DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return areItemsSame(oldList[oldItemPosition], newList[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }
}