package ir.sabaide.codecahllenge.utiles.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.sabaide.codecahllenge.network.AppClient
import ir.sabaide.codecahllenge.network.OkHttpClient
import ir.sabaide.codecahllenge.network.interceptors.CacheInterceptor
import ir.sabaide.codecahllenge.network.interceptors.HeaderInterceptor
import ir.sabaide.codecahllenge.network.utiles.GCFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {
    @Singleton
    @Provides
    fun provideOkHttp(headerInterceptor: HeaderInterceptor, cacheInterceptor: CacheInterceptor): okhttp3.OkHttpClient {
        return OkHttpClient(headerInterceptor, cacheInterceptor).build()
    }
}